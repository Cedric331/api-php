<?php
header('Content-Type: application/json; charset=utf-8');

// chargement des classes
function chargement($classe)
{
   $fichier = 'Classe/' . $classe . '.php';
   if (file_exists($fichier))
   {
      require $fichier;
   }
}
// Appel de la fonction pour charger les classes
spl_autoload_register('chargement');

// Appel de la classe DB et connexion de celle-ci
$db = new Db('localhost', 'produits', 'root', '');


$method = $_SERVER['REQUEST_METHOD'];

switch ($method) {
     case 'GET':
       if(!empty($_GET["id"]))
       {
         // Récupérer un seul produit
         $id = intval($_GET["id"]);
         echo $db->afficheOne($id);
       }
       else
       {
         // Récupérer tous les produits
        echo $db->afficheAll();
       }
       break;

       case 'POST':
         // Ajouter un produit
         $_POST = json_decode(file_get_contents('php://input'), true);
         $produit = array(
         'prix' => $_POST["prix"], 
         'titre' => $_POST["titre"], 
         'categorie' => $_POST["categorie"], 
         'contact' => $_POST["contact"]);
         echo $db->insert($produit);
         break;
      
      case 'PATCH':
         // Modifier un produit
         $_POST = json_decode(file_get_contents('php://input'), true);
         $produit = array(
         'prix' => $_POST["prix"], 
         'titre' => $_POST["titre"], 
         'categorie' => $_POST["categorie"], 
         'contact' => $_POST["contact"]);
         $id = intval($_GET["id"]);
         echo $db->update($id, $produit);
         break;

      case 'DELETE':
         // Supprimer un produit
         $id = intval($_GET["id"]);
         echo $db->deleteOne($id);
         break;

     default:
       // Requête invalide
       header("HTTP/1.0 405 Method Not Allowed");
       break;
   }









