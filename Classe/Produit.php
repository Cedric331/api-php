<?php 

Class Produit
{
   public $titre;
   public $prix;
   public $categorie;
   public $contact;

      public function __construct($titre,$prix,$categorie,$contact)
      {
          $this->titre = $titre;
          $this->prix = $prix;
          $this->categorie = $categorie;
          $this->contact = $contact;
      }

}