<?php

   class Db
   {

      // Connection
      private $connexion;

      // Choix de la table
      private $table;

      private $servername;
      private $username; 
      private $password; 


         // Db connection
         public function __construct($servername,$table, $username,$password)
         {
               $this->table = $table;
               $this->servername = $servername;
               $this->username = $username;
               $this->password = $password;
               $db = new PDO("mysql:host=$this->servername;dbname=$this->table", $this->username, $this->password);
               $this->connexion = $db;
         }

         // Insert dans la db un nouveau produit
         public function insert($produit)
         {
            $req = $this->connexion->prepare("INSERT INTO produits (titre, prix , categorie, contact) VALUES (?, ?, ?, ?)");

            $req->execute(array($produit['titre'], $produit['prix'], $produit['categorie'], $produit['contact']));

            if ($req == true) {
               return "Ajout dans la Table OK";
            }

               return "Erreur lors de l'insertion";
         }

         // récupère et affiche les produits
         public function afficheAll()
         {
            $sql = $this->connexion->query("SELECT * FROM produits", PDO::FETCH_ASSOC);
            foreach($sql as $value)
            {
               $data[] = $value;
            }
            return json_encode($data);
         }

         // récupère et affiche les produits
         public function afficheOne($id)
         {
            $sql = $this->connexion->query("SELECT * FROM produits WHERE id = $id ", PDO::FETCH_ASSOC);

               foreach($sql as $value)
               {
                 return $value = json_encode($value);
               }
            
         }

         // Affiche les produits compris entre la valeur min et max
         // public function affichePrix($min, $max)
         // {
         //       $sql = $this->connexion->query("SELECT * FROM produits WHERE prix BETWEEN $min AND $max", PDO::FETCH_ASSOC);
         //       foreach($sql as $value)
         //       {
         //          $data[] = $value;
         //       }
         //    return json_encode($data);
         // }

         public function update($id, $produit)
         {
            $req = $this->connexion->prepare("UPDATE produits SET titre = ?, prix = ?, categorie = ?, contact = ? WHERE id = $id ");

            $req->execute(array($produit['titre'], $produit['prix'], $produit['categorie'], $produit['contact']));

            return "Element modifié";
         }

         // récupère et affiche les produits
         public function deleteOne($id)
         {
            $req = $this->connexion->prepare("DELETE FROM produits WHERE id = :id ");
            $req->bindParam(':id', $id, PDO::PARAM_INT);
            $req->execute();
            return "Elément " . $id  . " supprimé";
         }
   }